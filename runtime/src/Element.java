

public class Element<T, TOuter extends Element<?,?>> {

	final protected TOuter outer;
	
	public Element(TOuter outer) {
		this.outer = outer;
	}
}
