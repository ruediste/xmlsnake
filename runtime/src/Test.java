
public class Test {

	public void test(){
		XmlCanvas.html<Element<?, ?>> html = new XmlCanvas.html<Element<?,?>>(null);
		
		// @formatter:off
		html
			.head()
				.title()._title()
			._head()
			.body()
				.div().Class("simple").Id("content")
					.p().Style("background: black;")
						.write("Hello")
					._p()
					.table().caption()._caption()
						.colgroup()
							.col()._col()
						._colgroup()
						.tr()
							.th().write("Hello World")._th()
							.th()._th()
						._tr()
					._table()
					.write("Foo")
				._div()
			._body()
		._html();
	}
}
