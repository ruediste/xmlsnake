package org.bitbucket.ruediste.xmlSnake;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Symbol {
	private static final Set<Symbol> symbols = new HashSet<Symbol>();

	public static final Symbol EPSILON = new SymbolImpl("E");
	public static final Symbol STRING = new SymbolImpl("S");

	protected Symbol() {
		symbols.add(this);
	}

	public static Set<Symbol> getSymbols() {
		return Collections.unmodifiableSet(symbols);
	}

	private static class SymbolImpl extends Symbol {
		private final String name;

		protected SymbolImpl(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

	}
}
