package org.bitbucket.ruediste.xmlSnake;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class DotGenerator {

	public void generate(Iterable<Element> elements, File outDir)
			throws IOException, InterruptedException {
		outDir.mkdirs();
		for (Element element : elements) {
			generate(element, outDir, "nfa");
			generate(element.determinized(), outDir, "dfa");
		}
	}

	private void generate(Element element, File outDir, String infix)
			throws FileNotFoundException, IOException, InterruptedException {
		String nameBase = element.getName() + "." + infix;
		File outFile = new File(outDir, nameBase + ".dot");
		PrintWriter out = new PrintWriter(outFile);
		out.println("digraph G {");
		generate(element, out);
		out.println("}");
		out.close();

		ProcessBuilder builder = new ProcessBuilder("dot", "-Tgif", "-o"
				+ nameBase + ".gif", nameBase + ".dot");
		builder.redirectErrorStream(true);
		builder.directory(outDir);
		Process process = builder.start();
		process.waitFor();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			System.out.println("Stdout: " + line);
		}
	}

	private void generate(Element element, PrintWriter out) {
		for (State n : element.getNodes()) {
			out.println(n.getName() + "[" + "label=\"" + n.toSimpleString()
					+ "\"];");
		}
		for (Transition t : element.getTransitions()) {
			out.println(t.getSource().getName() + " -> "
					+ t.getTarget().getName() + "[" + "label=\"" + t.getLabel()
					+ "\"];");
		}
	}
}
