package org.bitbucket.ruediste.xmlSnake;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class State {

	private boolean canReturn;
	private final Set<Transition> outgoingTransitions = new HashSet<Transition>();
	private final Set<Transition> incomingTransitions = new HashSet<Transition>();

	private String name;
	private final Element element;

	public State(Element element) {
		this.element = element;
		element.addNodeInv(this);
	}

	void removeOutgoingTransitionInv(Transition transition) {
		outgoingTransitions.remove(transition);
	}

	void addOutgoingTransitionInv(Transition transition) {
		outgoingTransitions.add(transition);
	}

	void removeIncomingTransitionInv(Transition transition) {
		incomingTransitions.remove(transition);
	}

	void addIncomingTransitionInv(Transition transition) {
		incomingTransitions.add(transition);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean canReturn() {
		return canReturn;
	}

	public void canReturn(boolean canReturn) {
		this.canReturn = canReturn;
	}

	public void absorb(State n) {
		for (Transition t : new ArrayList<Transition>(
				n.getIncomingTransitions())) {
			t.setTarget(this);
		}

		for (Transition t : new ArrayList<Transition>(
				n.getOutgoingTransitions())) {
			t.setSource(this);
		}

		n.destroy();
	}

	private void destroy() {
		element.removeNodeInv(this);
	}

	public Set<Transition> getOutgoingTransitions() {
		return Collections.unmodifiableSet(outgoingTransitions);
	}

	public Set<Transition> getIncomingTransitions() {
		return Collections.unmodifiableSet(incomingTransitions);
	}

	@Override
	public String toString() {
		return toSimpleString() + ":" + outgoingTransitions.toString();
	}

	public String toSimpleString() {
		if (canReturn)
			return name + "*";
		else
			return name;
	}
}
