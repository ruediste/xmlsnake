package org.bitbucket.ruediste.xmlSnake;

import java.util.HashMap;

import com.sun.xml.xsom.XSElementDecl;

public class ElementSymbol extends Symbol {

	private final XSElementDecl elementDecl;

	protected ElementSymbol(XSElementDecl elementDecl) {
		this.elementDecl = elementDecl;
	}

	@Override
	public String toString() {
		return elementDecl.getName();
	}

	private static HashMap<Pair<String, String>, ElementSymbol> map = new HashMap<Pair<String, String>, ElementSymbol>();

	public static ElementSymbol create(XSElementDecl elementDecl) {
		Pair<String, String> key = Pair.create(
				elementDecl.getTargetNamespace(), elementDecl.getName());
		if (map.containsKey(key))
			return map.get(key);

		ElementSymbol result = new ElementSymbol(elementDecl);
		map.put(key, result);
		return result;
	}

	public XSElementDecl getElementDecl() {
		return elementDecl;
	}

}
