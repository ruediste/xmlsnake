package org.bitbucket.ruediste.xmlSnake;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSSchema;

public class Element {
	private final Set<State> nodes = new LinkedHashSet<State>();
	private final Set<Transition> transitions = new LinkedHashSet<Transition>();
	private State startState;
	private String name;
	private XSSchema xmlSchema;
	private final Set<XSAttributeUse> attributes = new LinkedHashSet<XSAttributeUse>();

	void addNodeInv(State node) {
		nodes.add(node);
		if (nodes.size() == 1) {
			node.setName(name);
		} else {
			if (Character.isDigit(name.charAt(name.length() - 1))) {
				node.setName(name + "_" + nodes.size());
			} else {
				node.setName(name + nodes.size());
			}
		}
	}

	void addTransitionInv(Transition t) {
		transitions.add(t);
	}

	public Set<State> getNodes() {
		return Collections.unmodifiableSet(nodes);
	}

	public Set<Transition> getTransitions() {
		return Collections.unmodifiableSet(transitions);
	}

	public State getStartState() {
		return startState;
	}

	public void setStartState(State entryNode) {
		startState = entryNode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setXmlSchema(XSSchema schema) {
		xmlSchema = schema;
	}

	public XSSchema getXmlSchema() {
		return xmlSchema;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name + "[" + startState.getName() + "]" + " => ");
		for (State n : nodes) {
			sb.append(n).append(";");
		}
		sb.append("\n");
		for (XSAttributeUse a : attributes) {
			sb.append(a.getDecl().getName());
			sb.append(";");
		}
		return sb.toString();
	}

	void removeNodeInv(State node) {
		nodes.remove(node);
	}

	public void addAttribute(XSAttributeUse attribute) {
		attributes.add(attribute);
	}

	public Set<XSAttributeUse> getAttributes() {
		return Collections.unmodifiableSet(attributes);
	}

	/**
	 * Given a list of states the closure set is all the sets that can be
	 * reached without any input, including the input states themselves
	 */
	private Set<State> closure(Set<State> inputStates) {
		HashSet<State> found = new HashSet<State>();
		HashSet<State> todo = new HashSet<State>();
		todo.addAll(inputStates);
		found.addAll(inputStates);

		while (!todo.isEmpty()) {
			State state = todo.iterator().next();
			todo.remove(state);

			for (Transition t : state.getOutgoingTransitions()) {
				if (t.getSymbol() == Symbol.EPSILON) {
					if (!found.contains(t.getTarget())) {
						todo.add(t.getTarget());
						found.add(t.getTarget());
					}
				}
			}
		}
		return found;
	}

	/**
	 * Given a set of states and an input symbol find all the states reachable
	 * by traversing the edges that require the given input symbol. Then apply
	 * the closure algorithm on the output
	 */
	private Set<State> reachableStates(Set<State> inputStates, Symbol symbol) {
		HashSet<State> output = new HashSet<State>();
		for (State state : inputStates) {
			for (Transition t : state.getOutgoingTransitions()) {
				if (t.getSymbol() == symbol) {
					output.add(t.getTarget());
				}
			}
		}
		return closure(output);
	}

	public Element determinized() {
		// create result
		Element result = new Element();
		result.attributes.addAll(attributes);
		result.name = name;
		result.xmlSchema = xmlSchema;

		// do the determiniztion
		Map<Set<State>, State> found = new HashMap<Set<State>, State>();
		Set<Set<State>> todo = new HashSet<Set<State>>();
		{
			Set<State> startStates = closure(Collections.singleton(startState));
			State s = new State(result);
			s.canReturn(anyReturns(startStates));
			todo.add(startStates);
			found.put(startStates, s);
			result.setStartState(s);
		}

		while (!todo.isEmpty()) {
			Set<State> stateSet = todo.iterator().next();
			todo.remove(stateSet);

			for (Symbol symbol : Symbol.getSymbols()) {
				if (symbol == Symbol.EPSILON) {
					continue;
				}
				Set<State> reachableStates = reachableStates(stateSet, symbol);
				if (reachableStates.isEmpty()) {
					continue;
				}
				State s = found.get(reachableStates);
				if (s == null) {
					s = new State(result);
					s.canReturn(anyReturns(reachableStates));
					todo.add(reachableStates);
					found.put(reachableStates, s);
				}
				addTransitionIfNotExists(result, found.get(stateSet), s, symbol);
			}
		}

		return result;
	}

	private boolean anyReturns(Set<State> states) {
		for (State s : states)
			if (s.canReturn())
				return true;
		return false;
	}

	private void addTransitionIfNotExists(Element element, State source,
			State target, Symbol symbol) {
		// check for an existing transition
		for (Transition t : source.getOutgoingTransitions()) {
			if (t.getTarget() == target && t.getSymbol() == symbol)
				return;
		}

		// if no transistion is found, create a new one
		new Transition(element, source, target, symbol);
	}

	void removeTransitionInv(Transition transition) {
		transitions.remove(transition);
	}
}
