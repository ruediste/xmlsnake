package org.bitbucket.ruediste.xmlSnake;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSListSimpleType;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.XSUnionSimpleType;

public class Generator {

	public Map<String, String> namespaces = new HashMap<String, String>();

	public void addNamespace(String prefix, String URI) {
		namespaces.put(URI, prefix);
	}

	public String omitPrefix;

	public void generate(Iterable<Element> elements, File f) throws IOException {
		f.getParentFile().mkdirs();

		PrintWriter out = new PrintWriter(f, "UTF-8");
		out.println("import java.util.Date;");
		out.println("public class XmlCanvas {");
		for (Element element : elements) {
			generate(element.determinized(), out);
		}

		out.append("}\n");
		out.close();
	}

	private static class MethodSet {
		Map<String, Integer> methodNames = new HashMap<String, Integer>();

		public String getMethodName(String base) {
			Integer nr = methodNames.get(base);
			if (nr != null) {
				methodNames.put(base, nr + 1);
				return base + "_" + nr;
			}
			methodNames.put(base, 1);
			return base;
		}
	}

	private void generate(Element element, PrintWriter out) {
		for (State n : element.getNodes()) {
			MethodSet methodSet = new MethodSet();
			out.println("public static class " + n.getName()
					+ "<TOuter extends Element<?,?>> extends Element<"
					+ n.getName() + "<TOuter> ,TOuter> {");
			out.println("public " + n.getName() + "(TOuter outer){");
			out.println("  super(outer);\n}");
			for (Transition t : n.getOutgoingTransitions()) {
				// handle element references
				if (t.getSymbol() instanceof ElementSymbol) {
					ElementSymbol symbol = (ElementSymbol) t.getSymbol();

					String name = symbol.getElementDecl().getName();

					State target = t.getTarget();
					out.println("public " + name + "<" + target.getName()
							+ "<TOuter>> " + methodSet.getMethodName(name)
							+ "() {");
					out.println("  return new " + name + "<" + target.getName()
							+ "<TOuter>> (new " + target.getName()
							+ "<TOuter>(outer));");
					out.println("}");
				}

				// handle string content
				if (t.getSymbol() == Symbol.STRING) {
					out.println("public " + t.getTarget().getName()
							+ "<TOuter> write(String s){");
					out.println("  return new " + t.getTarget().getName()
							+ "<TOuter>(outer);");
					out.println("}");
				}
			}

			// add a return function
			if (n.canReturn()) {
				out.println("public TOuter _" + element.getName() + "(){");
				out.println("  return outer; ");
				out.println("}");
			}

			// add attributes functions for the first node
			if (element.getStartState() == n) {
				for (XSAttributeUse attribute : element.getAttributes()) {
					generateAttribute(element, methodSet, attribute, out);
				}
			}
			out.println("}");
			out.println();
		}
	}

	private void generateAttribute(Element element, MethodSet methodSet,
			XSAttributeUse attribute, PrintWriter out) {
		XSAttributeDecl decl = attribute.getDecl();

		if (!generateXsdType(element, methodSet, decl,
				decl.getTargetNamespace(), decl.getName(), out)) {
			handleType(element, methodSet, decl, decl.getType(), out);
		}

		// there is no type defined for the attribute, simply use a string
		// out.println(String.format("public %s %s(String string){",
		// getReturnType(element), getMethodName(attribute, methodSet)));
		// out.println("return this;}");
	}

	private void handleType(Element element, MethodSet methodSet,
			XSAttributeDecl attribute, XSType type, PrintWriter out) {

		if (generateXsdType(element, methodSet, attribute,
				type.getTargetNamespace(), type.getName(), out))
			return;

		if (type instanceof XSComplexType) {
			XSComplexType ct = (XSComplexType) type;

		}

		if (type instanceof XSSimpleType) {
			XSSimpleType st = (XSSimpleType) type;
			if (st instanceof XSListSimpleType) {
				XSListSimpleType list = (XSListSimpleType) st;
			}

			if (st instanceof XSRestrictionSimpleType) {
				XSRestrictionSimpleType restriction = (XSRestrictionSimpleType) st;
				handleType(element, methodSet, attribute,
						restriction.getBaseType(), out);
				return;
			}

			if (st instanceof XSUnionSimpleType) {
				XSUnionSimpleType union = (XSUnionSimpleType) st;
				for (XSSimpleType item : union) {
					handleType(element, methodSet, attribute, item, out);
				}
				return;
			}

		}
		throw new UnsupportedOperationException();
	}

	private boolean generateXsdType(Element element, MethodSet methodSet,
			XSAttributeDecl attribute, String nameSpace, String name,
			PrintWriter out) {
		if (nameSpace.equals(Translator.xsdNamespaceURI)) {
			String methodName = getMethodName(attribute, methodSet);
			String returnType = getReturnType(element);

			String[] stringTypes = { "string", "language", "token", "ID",
					"NCName", "NMTOKENS", "NMTOKEN", "anyURI", "IDREF",
					"IDREFS", "anySimpleType" };

			if (Arrays.asList(stringTypes).contains(name)) {
				out.println(String.format("public %s %s(String %s){",
						returnType, methodName, name));
				out.println("return this;}");
				return true;
			}

			String[] integerTypes = { "nonNegativeInteger" };

			if (Arrays.asList(integerTypes).contains(name)) {
				out.println(String.format("public %s %s(int %s){", returnType,
						methodName, name));
				out.println("return this;}");
				return true;
			}

			if (name.equals("dateTime")) {
				out.println(String.format("public %s %s(Date dateTime){",
						returnType, methodName));
				out.println("return this;}");
				return true;
			}

			throw new UnsupportedOperationException("unknow XSD type " + name
					+ " attribute: " + attribute);
		}
		return false;

	}

	private String getReturnType(Element element) {
		String returnType = element.getStartState().getName() + "<TOuter>";
		return returnType;
	}

	private String getMethodName(XSAttributeDecl attribute, MethodSet methodSet) {
		String prefix = namespaces.get(attribute.getTargetNamespace());
		if (prefix == null || prefix.equals(omitPrefix)) {
			prefix = "";
		}

		String methodName = cap1stChar(prefix)
				+ cap1stChar(attribute.getName());
		methodName = methodName.replace("-", "");
		methodName = Character.toUpperCase(methodName.charAt(0))
				+ methodName.substring(1);
		return methodSet.getMethodName(methodName);
	}

	public String cap1stChar(String s) {
		if (s.isEmpty())
			return s;
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}
}
