package org.bitbucket.ruediste.xmlSnake;

import com.google.common.base.Objects;

public class Pair<A, B> {

	private final A a;
	private final B b;

	public Pair(A a, B b) {
		super();
		this.a = a;
		this.b = b;
	}

	public A getA() {
		return a;
	}

	public B getB() {
		return b;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Pair))
			return false;
		Pair<?, ?> other = (Pair<?, ?>) obj;

		return Objects.equal(a, other.getA()) && Objects.equal(b, other.getB());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(a, b);
	}

	public static <A, B> Pair<A, B> create(A a, B b) {
		return new Pair<A, B>(a, b);
	}

}
