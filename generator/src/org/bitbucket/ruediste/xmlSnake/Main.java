package org.bitbucket.ruediste.xmlSnake;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.parser.XSOMParser;

public class Main {

	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException, ClassNotFoundException,
			InterruptedException {
		// String name = "test";
		String name = "xhtml1-strict";

		XSOMParser parser = new XSOMParser();
		parser.setEntityResolver(new EntityResolver() {

			@Override
			public InputSource resolveEntity(String publicId, String systemId)
					throws SAXException, IOException {
				System.out.println(publicId + " => " + systemId);
				if ("http://www.w3.org/XML/1998/namespace".equals(publicId))
					return new InputSource(new FileInputStream("xml.xsd"));
				return null;
			}
		});

		parser.parse(new File(name + ".xsd"));

		/*
		 * File fXmlFile = new File(name + ".xsd"); XmlSchemaCollection
		 * schemaCollection = new XmlSchemaCollection();
		 * 
		 * DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
		 * docFac.setNamespaceAware(true); final DocumentBuilder builder =
		 * docFac.newDocumentBuilder(); Document doc = builder.parse(new
		 * FileInputStream("xml.xsd")); schemaCollection.read(doc,
		 * "http://www.w3.org/2001/xml.xsd");
		 * 
		 * XmlSchema schema = schemaCollection.read(new InputSource( new
		 * FileInputStream(fXmlFile)));
		 */
		XSSchemaSet schemaSet = parser.getResult();
		XSSchema schema = schemaSet.getSchema(1);
		Translator t = new Translator();
		Set<Element> elements = t.translate(schema);
		// XmlSchemaElement root = schema.getElementByName("html");
		for (Element e : elements) {
			System.out.println(e);
		}

		Generator g = new Generator();
		g.addNamespace("xhtml", "http://www.w3.org/1999/xhtml");
		g.addNamespace("xml", "http://www.w3.org/XML/1998/namespace");
		g.addNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
		g.omitPrefix = "xhtml";
		g.generate(elements, new File("gen/XmlCanvas.java"));

		new DotGenerator().generate(elements, new File("dot/"));
	}
}
