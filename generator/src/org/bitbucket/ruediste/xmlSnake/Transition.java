package org.bitbucket.ruediste.xmlSnake;

public class Transition implements Cloneable {

	private State source;
	private State target;

	private final Element element;
	private final Symbol symbol;

	public Transition(Element element, State source, State target, Symbol symbol) {
		this.element = element;
		this.symbol = symbol;
		element.addTransitionInv(this);
		setSource(source);
		setTarget(target);
	}

	public State getSource() {
		return source;
	}

	public void setSource(State source) {
		if (source == null)
			throw new IllegalArgumentException("null not allowed as source");
		if (this.source != null) {
			this.source.removeOutgoingTransitionInv(this);
		}
		this.source = source;
		if (source != null) {
			source.addOutgoingTransitionInv(this);
		}
	}

	public State getTarget() {
		return target;
	}

	public void setTarget(State target) {
		if (target == null)
			throw new IllegalArgumentException("null not allowed as target");
		if (this.target != null) {
			this.target.removeIncomingTransitionInv(this);
		}
		this.target = target;
		if (target != null) {
			target.addIncomingTransitionInv(this);
		}
	}

	public String getLabel() {
		return symbol.toString();
	}

	@Override
	public String toString() {
		return getLabel() + " -> " + target.toSimpleString();
	}

	@Override
	public Transition clone() {
		try {
			Transition clone = (Transition) super.clone();
			element.addTransitionInv(clone);
			clone.setSource(source);
			clone.setTarget(target);
			return clone;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public void destory() {
		source.removeOutgoingTransitionInv(this);
		target.removeIncomingTransitionInv(this);
		element.removeTransitionInv(this);
	}

	public Symbol getSymbol() {
		return symbol;
	}
}
