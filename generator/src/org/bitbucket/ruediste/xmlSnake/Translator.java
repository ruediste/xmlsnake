package org.bitbucket.ruediste.xmlSnake;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Stack;

import org.xml.sax.Locator;

import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSComponent;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSListSimpleType;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.XSUnionSimpleType;
import com.sun.xml.xsom.XSWildcard;

public class Translator {
	public static final String xsdNamespaceURI = "http://www.w3.org/2001/XMLSchema";

	private class Context {
		Stack<Boolean> isMixed = new Stack<Boolean>();
	}

	public Set<Element> translate(XSSchema schema) {
		Set<Element> elements = new LinkedHashSet<Element>();

		Iterator<XSElementDecl> it = schema.iterateElementDecls();
		while (it.hasNext()) {
			Element element = handleElement(it.next(), schema);
			elements.add(element);
		}
		return elements;
	}

	private int indent = 0;
	private boolean log = false;

	private void printIndent() {
		for (int i = 0; i < indent * 4; i++) {
			System.out.print(" ");
		}
	}

	private void log(String s) {
		if (log) {
			printIndent();
			System.out.println(s);
		}
	}

	private void enter(String s, XSComponent component) {
		if (log) {
			Locator locator = component.getLocator();
			if (locator != null) {
				enter(s + "(" + locator.getLineNumber() + ")");
			} else {
				enter(s);
			}
		}
	}

	private void enter(String s) {
		if (log) {
			printIndent();
			System.out.println(">" + s);
			indent++;
		}
	}

	private void leave(String s) {
		if (log) {
			indent--;
			printIndent();
			System.out.println("<" + s);
		}
	}

	private Element handleElement(XSElementDecl xsElementDecl, XSSchema schema) {
		try {
			enter(xsElementDecl.getName(), xsElementDecl);
			Context ctx = new Context();
			Element element = new Element();
			element.setName(xsElementDecl.getName());
			element.setXmlSchema(schema);
			State entryNode = new State(element);
			element.setStartState(entryNode);

			handleType(ctx, element, entryNode, xsElementDecl.getType());
			leave(xsElementDecl.getName());
			return element;
		} catch (Throwable t) {
			throw new RuntimeException("Error while handling element "
					+ xsElementDecl.getName(), t);
		}

	}

	private State handleType(Context ctx, Element element, State entryNode,
			XSType type) {
		enter("type " + type.getName(), type);
		try {
			if (type.getBaseType() != null && type.getBaseType() != type) {
				log("base type skipped");
				// handleType(ctx, element, entryNode, type.getBaseType());
			}

			if (type instanceof XSSimpleType) {
				log("simple");
				XSSimpleType st = (XSSimpleType) type;

				if (st.getTargetNamespace().equals(xsdNamespaceURI)
						&& st.getName().equals("string")) {
					// the content of the node is defined as string
					State n = new State(element);
					n.canReturn(true);

					new Transition(element, entryNode, n, Symbol.STRING);
					return n;
				}
				if (type instanceof XSListSimpleType) {
					XSListSimpleType l = (XSListSimpleType) type;
				} else if (type instanceof XSRestrictionSimpleType) {
					XSRestrictionSimpleType l = (XSRestrictionSimpleType) type;
				} else if (type instanceof XSUnionSimpleType) {
					XSUnionSimpleType l = (XSUnionSimpleType) type;
				} else {
					// TransitionElement t=new TransitionElement(st, element);
				}
				throw new UnsupportedOperationException();
			}

			if (type instanceof XSComplexType) {
				log("complex");
				XSComplexType ct = (XSComplexType) type;

				ctx.isMixed.push(ct.isMixed());

				// handle attributes
				addAttributes(element, ct.getAttributeUses());

				// handle content
				XSContentType contentType = ct.getContentType();
				State n = entryNode;
				if (contentType instanceof XSParticle) {
					n = handleParticle(ctx, element, entryNode,
							contentType.asParticle());
					n.canReturn(true);
				} else {
					n.canReturn(true);
				}

				ctx.isMixed.pop();
				return n;
			}
			throw new UnsupportedOperationException();
		} catch (Throwable t) {
			throw new RuntimeException("Error while handling type "
					+ type.getTargetNamespace() + "::" + type.getName(), t);
		} finally {
			leave("type" + type.getName());
		}
	}

	private void addAttributes(Element element,
			Iterable<? extends XSAttributeUse> attributes) {
		for (XSAttributeUse attr : attributes) {
			addAttribute(element, attr);
		}
	}

	private void addAttribute(Element element, XSAttributeUse attributeUse) {
		element.addAttribute(attributeUse);
	}

	private State handleParticle(Context ctx, Element element, State startNode,
			XSParticle particle) {
		enter("particle " + particle, particle);
		try {
			XSTerm term = particle.getTerm();

			State endNode = handleTerm(ctx, element, startNode, term);
			if (particle.getMaxOccurs().compareTo(BigInteger.ONE) > 0
					|| particle.getMaxOccurs()
							.compareTo(BigInteger.valueOf(-1)) == 0) {
				startNode.absorb(endNode);
				return startNode;
			}

			if (particle.getMinOccurs().compareTo(BigInteger.ZERO) == 0) {
				new Transition(element, startNode, endNode, Symbol.EPSILON);
			}
			return endNode;
		} catch (Throwable t) {
			throw new RuntimeException("Error while handling particle "
					+ particle, t);
		} finally {
			leave("particle " + particle);
		}

	}

	private State handleTerm(Context ctx, Element element, State startNode,
			XSTerm term) {
		enter("term " + term, term);
		try {
			if (term instanceof XSModelGroup) {
				XSModelGroup modelGroup = (XSModelGroup) term;
				switch (modelGroup.getCompositor()) {
				case ALL:
					throw new UnsupportedOperationException();
				case CHOICE: {
					State endNode = startNode;
					for (XSParticle item : ((XSModelGroup) term).getChildren()) {
						State n = handleParticle(ctx, element, startNode, item);
						if (endNode == startNode) {
							endNode = n;
						} else {
							endNode.absorb(n);
						}
					}
					return endNode;
				}
				case SEQUENCE: {
					State endNode = startNode;
					for (XSParticle item : ((XSModelGroup) term).getChildren()) {
						endNode = handleParticle(ctx, element, endNode, item);
					}
					return endNode;
				}
				default:
					throw new UnsupportedOperationException();
				}

			}
			if (term instanceof XSModelGroupDecl)
				return handleTerm(ctx, element, startNode,
						((XSModelGroupDecl) term).getModelGroup());
			if (term instanceof XSWildcard)
				// nothing to do
				return startNode;

			if (term instanceof XSElementDecl) {
				XSElementDecl elementDecl = (XSElementDecl) term;
				State n = new State(element);
				new Transition(element, startNode, n,
						ElementSymbol.create(elementDecl));

				if (ctx.isMixed.peek()) {
					new Transition(element, startNode, n, Symbol.STRING);
				}
				return n;
			}

			throw new UnsupportedOperationException("Unhandled Term " + term);
		} catch (Throwable t) {
			throw new RuntimeException("error while handling term " + term, t);
		} finally {
			leave("term " + term);
		}
	}
}
